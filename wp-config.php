<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'scarsticks' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Vj0!o zbvB9ZpdwT@HQlD}w@+{z8jN)pVQsS(A423i%^k5;W/s*o/[e*hD<Ki03m' );
define( 'SECURE_AUTH_KEY',  'TjfQ0bykQJcAr%?RgC.l`}i&>HC9PPbKs#k4o0^V}hP` eF:B3D:bx;W^va;+a5;' );
define( 'LOGGED_IN_KEY',    '+DHe[-vBANc}4[l/,9>%y=,J4!CLHK.FIV@w8GxTe[`>:u~ @<IK*lbT&63n?q,V' );
define( 'NONCE_KEY',        'X}WMkqcvF,GiZn<v[Fy)%Qq*#f<}uI]<A:ZwE:w_8G}Y uoFg;~<:/^qq>+oQ9_-' );
define( 'AUTH_SALT',        'Uo)S!3$O%WAN|--e1NL;j[K*9F54[bxqySSxs@HSN4_=.9*a6Y;E!R<Ol%B>8N,u' );
define( 'SECURE_AUTH_SALT', 'lZOl8YrOO{89dy~0w+Fp+TzJyI>r6@)MyPwZQGf}(SB6<Y_.,Y9ky?tGecIYReQS' );
define( 'LOGGED_IN_SALT',   'tvG!p3U>d<ERDMRrTF9H_.:*E/V/23`L+VNb_ne89q.- Qjz7bIIgkEJHwN#aJ}&' );
define( 'NONCE_SALT',       '7]j Z3&h`yK4^@[Oe0s();utqA]/xmXH6#APAAe6#n]fESVM@||JMaW%.7h+Xw^Z' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
